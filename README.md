![alt text](pictures/banner.webp "Shadowsocks")  
[![pipeline status](https://gitlab.com/yiguihai/ss/badges/dev/pipeline.svg)](https://gitlab.com/yiguihai/ss/-/commits/dev)  
**不支持Centos**  
### 使用方法
安装脚本
```Shell
wget --no-check-certificate -O /usr/local/bin/ss-main 'https://gitlab.com/yiguihai/ss/-/raw/dev/usr/bin/ss-main'
chmod +x /usr/local/bin/ss-main
```
安装脚本(CDN)
```Shell
wget --no-check-certificate -O /usr/local/bin/ss-main 'https://glcdn.githack.com/yiguihai/ss/-/raw/dev/usr/bin/ss-main'
chmod +x /usr/local/bin/ss-main
```
运行脚本
```Shell
ss-main
```
<details open>
  <summary>更新记录</summary>
  <table>
    <caption><i><b>2024-05-08 07:07:44</b></i></caption>
    <thead>
      <tr>
        <th>项目</th>
        <th>更新详情</th>
      </tr>
    </thead>
    <tbody>
      
<tr><td><a href="https://quic.nginx.org">nginx</a></td><td><a href="https://hg.nginx.org/nginx/rev/49dce50fad40">release-1.25.5 tag</a></td></tr>
<tr><td><a href="https://quic.nginx.org">nginx</a></td><td><a href="https://hg.nginx.org/nginx/rev/6c8595b77e66">QUIC: path aware in-flight bytes accounting.</a></td></tr>
<tr><td><a href="https://quic.nginx.org">nginx</a></td><td><a href="https://hg.nginx.org/nginx/rev/89093b003fcb">Stream pass: disabled passing from or to udp.</a></td></tr>
<tr><td><a href="https://quic.nginx.org">nginx</a></td><td><a href="https://hg.nginx.org/nginx/rev/b05c622715fa">HTTP: uniform checks in ngx_http_alloc_large_header_buffer().</a></td></tr>
<tr><td><a href="https://quic.nginx.org">nginx</a></td><td><a href="https://hg.nginx.org/nginx/rev/cc16989c6d61">QUIC: fixed format specifier after a6f79f044de5.</a></td></tr>
<tr><td><a href="https://quic.nginx.org">nginx</a></td><td><a href="https://hg.nginx.org/nginx/rev/cdf74ac25b47">release-1.26.0 tag</a></td></tr>
<tr><td><a href="https://quic.nginx.org">nginx</a></td><td><a href="https://hg.nginx.org/nginx/rev/ee40e2b1d083">SSL: reasonable version for LibreSSL adjusted.</a></td></tr>
<tr><td><a href="https://quic.nginx.org">nginx</a></td><td><a href="https://hg.nginx.org/nginx/rev/f366007dd23a">HTTP/3: added Huffman decoding error logging.</a></td></tr>
<tr><td>kcptun</td><td><a href=https://github.com/xtaci/kcptun/commit/13f48e04df32397db736fb0c51c2058399d0b961>add vendor</a></td></tr>
<tr><td>kcptun</td><td><a href=https://github.com/xtaci/kcptun/commit/15e358a5e8ff4fdd83b3f3eac75accff2eb44dcf>upd deps to kcp-go@v5.6.8</a></td></tr>
<tr><td>kcptun</td><td><a href=https://github.com/xtaci/kcptun/commit/684254323620f95f963c95d8c72eb6738188d477>Bump golang.org/x/net from 0.19.0 to 0.23.0 (#944)</a></td></tr>
<tr><td>kcptun</td><td><a href=https://github.com/xtaci/kcptun/commit/8929bb029dc0e9cee0c3103331bd0d25ba2550ff>upd pgo</a></td></tr>
<tr><td>kcptun</td><td><a href=https://github.com/xtaci/kcptun/commit/a9225382a8966e49ec80815bdf9a0a0bffc3e3c5>Update README.md</a></td></tr>
<tr><td>libqrencode</td><td><a href=https://github.com/fukuchi/libqrencode/commit/715e29fd4cd71b6e452ae0f4e36d917b43122ce8>Level check failure in QRinput_new2() on Windows has been fixed.</a></td></tr>
<tr><td>php</td><td><a href="https://www.php.net/downloads.php">php-8.3.6</a></td></tr>
<tr><td>shadowsocks-rust</td><td><a href=https://github.com/shadowsocks/shadowsocks-rust/commit/0b62554e1de3bdc2afc3ffe98ca679680523bee0>chore(deps): bump webpki-roots from 0.25.2 to 0.25.3 (#1363)</a></td></tr>
<tr><td>shadowsocks-rust</td><td><a href=https://github.com/shadowsocks/shadowsocks-rust/commit/0e07a3a592686ca6aacdb8c8c4b5e0aebdf461f3>chore(deps): bump clap from 4.4.11 to 4.4.12 (#1401)</a></td></tr>
<tr><td>shadowsocks-rust</td><td><a href=https://github.com/shadowsocks/shadowsocks-rust/commit/2db40d8c7b4c26974ad9878e10b6c897c2bf942c>chore(deps): bump windows-sys from 0.48.0 to 0.52.0 (#1358)</a></td></tr>
<tr><td>shadowsocks-rust</td><td><a href=https://github.com/shadowsocks/shadowsocks-rust/commit/3b2cd405d004289394cacde40626efc95ed4d057>fix(local): dns-over-tls/https with rustls add dependencies to certs</a></td></tr>
<tr><td>shadowsocks-rust</td><td><a href=https://github.com/shadowsocks/shadowsocks-rust/commit/43506cf95f10a511b5caedae57fe79725777b59e>chore(deps): bump env_logger from 0.10.0 to 0.10.1 (#1352)</a></td></tr>
<tr><td>shadowsocks-rust</td><td><a href=https://github.com/shadowsocks/shadowsocks-rust/commit/4d0afeac6a29c84ac9a5680f9e28a1e133b5d9a6>Set the incoming socket buffer sizes properly on the listen socket. (#1381)</a></td></tr>
<tr><td>shadowsocks-rust</td><td><a href=https://github.com/shadowsocks/shadowsocks-rust/commit/5c7668e0c4ccfd9d082f6eba581d54855ea8dd10>chore: update dependencies</a></td></tr>
<tr><td>shadowsocks-rust</td><td><a href=https://github.com/shadowsocks/shadowsocks-rust/commit/76b0239879b59df516a35eab947818e5ade622b2>reformatted</a></td></tr>
<tr><td>shadowsocks-rust</td><td><a href=https://github.com/shadowsocks/shadowsocks-rust/commit/7f0779a81ed601a2827eb08040934ead97b79002>chore(deps): bump sysexits from 0.7.7 to 0.7.10 (#1404)</a></td></tr>
<tr><td>shadowsocks-rust</td><td><a href=https://github.com/shadowsocks/shadowsocks-rust/commit/846752866e0b52c3d93efa2036204eadc36cc696>release v1.18.3</a></td></tr>
<tr><td>shadowsocks-rust</td><td><a href=https://github.com/shadowsocks/shadowsocks-rust/commit/a0c6ff8be1da66ae2ff199034b76fcef82b2f931>update dependencies</a></td></tr>
<tr><td>shadowsocks-rust</td><td><a href=https://github.com/shadowsocks/shadowsocks-rust/commit/bb3b343ae563ed7ca09370f459f36a97a3f8ea53>fix: local-dns UDP remote client fails if packet_id overflows</a></td></tr>
<tr><td>shadowsocks-rust</td><td><a href=https://github.com/shadowsocks/shadowsocks-rust/commit/f7a48e8af856a554c72da427c2c57a46158de725>update dependencies</a></td></tr>
<tr><td>tun2socks</td><td><a href=https://github.com/xjasonlyu/tun2socks/commit/0d819e1aec78165ebe55fffd438121496def1c61>Fix: codeql autobuild error (#355)</a></td></tr>
<tr><td>tun2socks</td><td><a href=https://github.com/xjasonlyu/tun2socks/commit/c8f8cb5caf6796039a08d3ebad5354767795628b>Chore(release/go): use version from go.mod (#317)</a></td></tr>
<tr><td>tun2socks</td><td><a href=https://github.com/xjasonlyu/tun2socks/commit/e86b3b7dc56f03993786114c0e075af3a428b490>Perf(SOCKS5): optimize memory footprint with authentication (#315)</a></td></tr>
<tr><td>v2ray-plugin</td><td><a href=https://github.com/teddysun/v2ray-plugin/commit/172c1c527be6a9fb7542b8d86b5a8815c1d21511>Update README.md</a></td></tr>
<tr><td>v2ray-plugin</td><td><a href=https://github.com/teddysun/v2ray-plugin/commit/b78f1a64ac773b9bdadedc12080c3a3ab1ffd18f>Update v2ray-core to v5.15.1</a></td></tr>
<tr><td>v2ray-plugin</td><td><a href=https://github.com/teddysun/v2ray-plugin/commit/c221d8e0c8fccf5f3e8283e551ab3bac71493f61>Update v2ray-core to v5.13.0</a></td></tr>
<tr><td>v2ray</td><td><a href=https://github.com/v2fly/v2ray-core/commit/1c103e6179964631d9f21bde024fea861f637b8d>Fix ss2022 auth reader size overflow</a></td></tr>
<tr><td>v2ray</td><td><a href=https://github.com/v2fly/v2ray-core/commit/2a22a8579d485498b2f423af5073fcbcb784c351>Update version to v5.15.3</a></td></tr>
<tr><td>v2ray</td><td><a href=https://github.com/v2fly/v2ray-core/commit/346ca66c57ab1b6872908354d5f3e4152fff59da>updated version to v5.16.1</a></td></tr>
<tr><td>v2ray</td><td><a href=https://github.com/v2fly/v2ray-core/commit/458db3954f76bbf7ce621a24151d9d5469d8b6fb>update version to v5.13.0</a></td></tr>
<tr><td>v2ray</td><td><a href=https://github.com/v2fly/v2ray-core/commit/5127b1635af9e789ed7a443c3a7a2e540429e24e>Chore: bump golang.org/x/net from 0.18.0 to 0.19.0</a></td></tr>
<tr><td>v2ray</td><td><a href=https://github.com/v2fly/v2ray-core/commit/62428d8011df4c4f590d48bde6aa1513e7a92c20>update version to v5.12.0</a></td></tr>
<tr><td>v2ray</td><td><a href=https://github.com/v2fly/v2ray-core/commit/77978bc2073d7e215992eaf3b8e6e8e852a716a7>remove semgrep</a></td></tr>
<tr><td>v2ray</td><td><a href=https://github.com/v2fly/v2ray-core/commit/79b2b1f3731d13dbb1b699d0de7fde2fbd478810>Chore: bump github.com/quic-go/quic-go from 0.42.0 to 0.43.0</a></td></tr>
<tr><td>v2ray</td><td><a href=https://github.com/v2fly/v2ray-core/commit/8191faa6e0951fd1c84864ce7780863142104b22>Add pie build mode to all binary builds</a></td></tr>
<tr><td>v2ray</td><td><a href=https://github.com/v2fly/v2ray-core/commit/b91354901c90f472b8806f1f81dce38f848e483e>update version to v5.12.1</a></td></tr>
    </tbody>
  </table>
</details>
