![alt text](pictures/banner.webp "Shadowsocks")  
[![pipeline status](https://gitlab.com/yiguihai/ss/badges/dev/pipeline.svg)](https://gitlab.com/yiguihai/ss/-/commits/dev)  
**不支持Centos**  
### 使用方法
安装脚本
```Shell
wget --no-check-certificate -O /usr/local/bin/ss-main 'https://gitlab.com/yiguihai/ss/-/raw/dev/usr/bin/ss-main'
chmod +x /usr/local/bin/ss-main
```
安装脚本(CDN)
```Shell
wget --no-check-certificate -O /usr/local/bin/ss-main 'https://glcdn.githack.com/yiguihai/ss/-/raw/dev/usr/bin/ss-main'
chmod +x /usr/local/bin/ss-main
```
运行脚本
```Shell
ss-main
```
<details open>
  <summary>更新记录</summary>
  <table>
    <caption><i><b></b></i></caption>
    <thead>
      <tr>
        <th>项目</th>
        <th>更新详情</th>
      </tr>
    </thead>
    <tbody>
      
<tr><td><a href="https://quic.nginx.org">nginx</a></td><td><a href="https://hg.nginx.org/nginx/rev/3db945fda515">QUIC: handle callback errors in compat.</a></td></tr>
<tr><td><a href="https://quic.nginx.org">nginx</a></td><td><a href="https://hg.nginx.org/nginx/rev/77c1418916f7">QUIC: use AEAD to encrypt address validation tokens.</a></td></tr>
<tr><td>ipt2socks</td><td><a href=https://github.com/zfl9/ipt2socks/commit/6dd16a93fb722b75192c07232c9755ed117d4fd1>Update README.md</a></td></tr>
<tr><td>kcptun</td><td><a href=https://github.com/xtaci/kcptun/commit/432e75c76ad7534a486d30ed6ed35051dba0eb53>Bump golang.org/x/net from 0.5.0 to 0.7.0 (#910)</a></td></tr>
<tr><td>kcptun</td><td><a href=https://github.com/xtaci/kcptun/commit/55e670033ba376e35d20d87fab20efbf76c233fb>add default.pgo in client & added an option to enable pprof on client</a></td></tr>
<tr><td>libqrencode</td><td><a href=https://github.com/fukuchi/libqrencode/commit/715e29fd4cd71b6e452ae0f4e36d917b43122ce8>Level check failure in QRinput_new2() on Windows has been fixed.</a></td></tr>
<tr><td>php</td><td><a href="https://www.php.net/downloads.php">php-8.2.11</a></td></tr>
<tr><td>php</td><td><a href="https://www.php.net/downloads.php">php-8.2.7</a></td></tr>
<tr><td>shadowsocks-rust</td><td><a href=https://github.com/shadowsocks/shadowsocks-rust/commit/59c0af07cdc266db6126d8f6ca9fc7176b96496d>EOPNOTSUPP and ENOTSUP have exactly the same value on Linux</a></td></tr>
<tr><td>shadowsocks-rust</td><td><a href=https://github.com/shadowsocks/shadowsocks-rust/commit/b25e9e36af4d69f5579c514305ade9cfa24fd2c4>bugfix: fix missed client_cache_size config item in SSLocalExtConfig (#1319)</a></td></tr>
<tr><td>shadowsocks-rust</td><td><a href=https://github.com/shadowsocks/shadowsocks-rust/commit/ea7346c5764e9eb8b6150b030610e9e4624aa205>chore(deps): bump clap from 4.4.4 to 4.4.6 (#1314)</a></td></tr>
<tr><td>shadowsocksr-libev</td><td><a href=https://github.com/shadowsocksrr/shadowsocksr-libev/commit/4799b312b8244ec067b8ae9ba4b85c877858976c>Merge pull request #49 from KazamaSion/patch-2</a></td></tr>
<tr><td>simple-obfs</td><td><a href=https://github.com/shadowsocks/simple-obfs/commit/486bebd9208539058e57e23a12f23103016e09b4>Merge pull request #252 from rogers0/PR/fix_gcc9</a></td></tr>
<tr><td>trojan</td><td><a href=https://github.com/trojan-gfw/trojan/commit/3e7bb9aecdc694f9bcae8d646fae395f773d60f8>Update CONTRIBUTORS.md</a></td></tr>
<tr><td>tun2socks</td><td><a href=https://github.com/xjasonlyu/tun2socks/commit/68da4d999713e9e69d284f907d6535386c00fe22>Chore: update go mod (#303)</a></td></tr>
<tr><td>tun2socks</td><td><a href=https://github.com/xjasonlyu/tun2socks/commit/8309fddef324b49fcc26029298f99829635d50d7>Feature: fdbased open fd with offset (#272)</a></td></tr>
<tr><td>v2ray-plugin</td><td><a href=https://github.com/teddysun/v2ray-plugin/commit/68e1d7dd0ea60812a2b61e34ce22c0fb81af1497>Update v2ray-core to v5.8.0</a></td></tr>
<tr><td>v2ray-plugin</td><td><a href=https://github.com/teddysun/v2ray-plugin/commit/ae6ce313002c3f55a074dd3e0b6581afb3f8d715>Update v2ray-core to v5.7.0</a></td></tr>
<tr><td>v2ray</td><td><a href=https://github.com/v2fly/v2ray-core/commit/5c995d97f43e8e97ee0cb3d1aa12450d3968c8d2>fix: panic in dns over quic when address is a ip</a></td></tr>
<tr><td>v2ray</td><td><a href=https://github.com/v2fly/v2ray-core/commit/ed7e4d00f34be0a11b3f299d4a754538daf56275>Chore: bump github.com/refraction-networking/utls from 1.5.2 to 1.5.3 (#2682)</a></td></tr>
    </tbody>
  </table>
</details>
