#!/bin/sh

export PATH=$PATH:/root

Get_Config() {
	local i4 i6
	i4=$(ip -4 -o route get to 8.8.8.8 2>/dev/null)
	i6=$(ip -6 -o route get to 2001:4860:4860::8888 2>/dev/null)
	lan_device=$(uci -q get network.lan.device)
	firewall_forward=$(uci -q get firewall.@defaults[0].forward)
	#ip address|awk '/inet / {print $2}'
	if [ "$i4" ]; then
		ipv4=$(echo "$i4" | sed -n 's/.*src \([0-9.]\+\).*/\1/p')
		internet4=$(echo "$i4" | sed -En 's/.*dev ([^ ]+).*/\1/p')
	fi
	if [ "$i6" ]; then
		ipv6=$(echo "$i6" | sed -n 's/.*src \([^ ]*\).*/\1/p')
		internet6=$(echo "$i6" | sed -En 's/.*dev ([^ ]+).*/\1/p')
	fi
}

Start_Proxy_Process() {
	hev-socks5-tunnel main.yml
	sslocal --config ss.json --acl bypass-china.acl --user nobody --daemonize --daemonize-pid /tmp/ss.pid
}

Stop_Proxy_Process() {
	for i in /tmp/ss.pid /tmp/hev-socks5-tunnel.pid; do
		[ -s "$i" ] && read -r kpid <"$i"
		if [ -d /proc/"${kpid:=lzbx}" ]; then
			kill "$kpid"
			if [ -f "$i" ]; then
				rm -f "$i"
			fi
		fi
	done
}

Start_Forward_Rule() {
	if [ "$firewall_forward" ] && [ "$firewall_forward" != 'ACCEPT' ]; then
		uci set firewall.@defaults[0].forward='ACCEPT' #openwrt环境里默认是reject的这里一定改成accept不然无法转发局域网设备的流量被坑惨了。以后配置规则一定要认真检查原有的防火墙规则！
		uci commit firewall
	fi
	if [ "$internet4" ] || [ "$internet6" ]; then
		if nft --file nftables.conf; then
			if [ "$ipv4" ] && [ "$internet4" ]; then
				ip rule add fwmark 0x2233 table 123
				ip route add default dev tun0 table 123
			fi
			if [ "$ipv6" ] && [ "$internet6" ]; then
				ip -6 rule add fwmark 0x2233 table 123
				ip -6 route add default dev tun0 table 123
			fi
			if [ "$lan_device" ]; then
				nft add rule inet shadowsocks prerouting iifname "${lan_device:=br-lan}" counter goto tunnel_forward
			fi
			if [ "$internet4" ]; then
				nft add rule inet shadowsocks output oifname "$internet4" counter goto tunnel_forward
			fi
			if [ "$internet6" ] && [ "$internet6" != "$internet4" ]; then
				nft add rule inet shadowsocks output oifname "$internet6" counter goto tunnel_forward
			fi
		fi
	fi
}

Stop_Forward_Rule() {
	if [ "$ipv4" ] && [ "$internet4" ]; then
		ip rule del fwmark 0x2233 table 123
		ip route del default dev tun0 table 123
	fi
	if [ "$ipv6" ] && [ "$internet6" ]; then
		ip -6 rule del fwmark 0x2233 table 123
		ip -6 route del default dev tun0 table 123
	fi
	nft flush table inet shadowsocks
	nft delete table inet shadowsocks
}

Get_Config
Start_Proxy_Process
Start_Forward_Rule
