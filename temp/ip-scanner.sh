#!/bin/bash
cd /tmp

git clone --depth 1 https://mirror.ghproxy.com/https://github.com/ip-scanner/cloudflare folder1
#e.g https://github.com/ip-scanner/cloudflare/archive/refs/heads/main.tar.gz or .zip
git clone --depth 1 https://mirror.ghproxy.com/https://github.com/ymyuuu/IPDB folder2
git clone --depth 1 https://mirror.ghproxy.com/https://github.com/hello-earth/cloudflare-better-ip folder3
grep -Eo '([0-9]+\.){3}[0-9]+(/[0-9]{1,2})?' folder1/*.csv folder2/*.txt folder3/cloudflare/*.txt | grep -Ev '^192\.168|^172\.1[6-9]\.|^172\.2[0-9]\.|^172\.3[0-2]\.|^10\.|^127\.|^255\.|^0\.' | sort | uniq > ipv4_filtered.txt
grep -Eo '(([0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|(([0-9a-fA-F]{1,4}:){6})([0-9.]+))/[0-9]+' folder1/*.csv folder2/*.txt folder3/cloudflare/*.txt | grep -Ev '^::1/128' | sort | uniq > ipv6_filtered.txt

numCompare() {
	return $(echo | awk "{ print ($1 >= $2)?0 : 1 }")
}
Traffic() {
	local i=${1%.*}
	if [ "$i" -lt 1024 ]; then
		printf '%d B' "$i"
	elif [ "$i" -lt $((1024 ** 2)) ]; then
		awk 'BEGIN{printf "%.2f KB",('"$i"' / 1024)}'
	elif [ "$i" -lt $((1024 ** 3)) ]; then
		awk 'BEGIN{printf "%.2f MB",('"$i"' / (1024 ^ 2))}'
	elif [ "$i" -lt $((1024 ** 4)) ]; then
		awk 'BEGIN{printf "%.2f GB",('"$i"' / (1024 ^ 3))}'
	elif [ "$i" -lt $((1024 ** 5)) ]; then
		awk 'BEGIN{printf "%.2f TB",('"$i"' / (1024 ^ 4))}'
	elif [ "$i" -lt $((1024 ** 6)) ]; then
		awk 'BEGIN{printf "%.2f PB",('"$i"' / (1024 ^ 5))}'
	elif [ "$i" -lt $((1024 ** 7)) ]; then
		awk 'BEGIN{printf "%.2f EB",('"$i"' / (1024 ^ 6))}'
	elif [ "$i" -lt $((1024 ** 8)) ]; then
		awk 'BEGIN{printf "%.2f ZB",('"$i"' / (1024 ^ 7))}'
	elif [ "$i" -lt $((1024 ** 9)) ]; then
		awk 'BEGIN{printf "%.2f YB",('"$i"' / (1024 ^ 8))}'
	elif [ "$i" -lt $((1024 ** 10)) ]; then
		awk 'BEGIN{printf "%.2f BB",('"$i"' / (1024 ^ 9))}'
	fi
}

Check() {
    echo -e "${x}/${y} \r"
	local response_code=$(curl -s -o /dev/null -w '%{response_code}' --connect-timeout 3 --max-time 6 --resolve cp.cloudflare.com:443:${line#*:} https://cp.cloudflare.com/generate_204)
	if [ ${response_code:=000} -eq 204 ]; then
		IFS='|' read -r -a array <<<"$(curl -s -o /dev/null -w '%{response_code}|%{content_type}|%{speed_download}' --user-agent "MAUI WAP Browser" --connect-timeout 5 --max-time 15 --resolve loveguihai.buzz:443:${line#*:} https://loveguihai.buzz/1M.png)"
		if [ ${array[0]:-0} -eq 200 ] && [ "${array[1]:=xxx}" = "image/png" ]; then
			echo "${line#*:}" >>ip.txt
			speed_download=$(Traffic ${array[2]:-0})
			echo "${line#*:} ${speed_download:-0}/s"
			#echo "${str1##*/} ${x}, ${line#*:}, ${speed_download% *}, ${speed_download#* }" >>ip2.csv
			#下载速度小于1MB/s的都可以滚了
			if numCompare ${speed_download% *} 1 && [ "${speed_download#* }" = "MB" ]; then
				echo "${x}, ${line}, ${speed_download% *}, ${speed_download#* }" >>ip2.csv
			fi
		fi
	fi
}

for i in ipv4_filtered.txt; do
	x=0
	y=0
	while IFS= read -r line || [ -n "$line" ]; do
	  ((y++))
	done <"$i"
	while IFS= read -r line || [ -n "$line" ]; do
		((x++))
		if [ $x -le 9999 ]; then
			Check &
			xx+=(${!})
		else
			break
		fi
		if [ ${#xx[*]} -ge 5 ]; then
			while true; do
				m=0
				for num in ${xx[@]}; do
					if [ -d /proc/"${num:-000}" ]; then
						((m++))
					fi
				done
				if [ $m -gt 0 ]; then
					sleep 1
				else
					break
				fi
			done
			xx=()
		fi
	done <"$i"
done