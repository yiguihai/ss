table inet shadowsocks {
    set proxy_port {           
        type inet_service;
        elements = {
          "ssh",
          "http",
          "https",
          1080
        }
    }
    set byp4 {
        typeof ip daddr
        flags interval
        elements = { 
          0.0.0.0/8, 
          10.0.0.0/8, 
          127.0.0.0/8, 
          169.254.0.0/16, 
          172.16.0.0/12, 
          192.0.0.0/24, 
          192.0.2.0/24, 
          192.88.99.0/24, 
          192.168.0.0/16, 
          198.18.0.0/15, 
          198.51.100.0/24, 
          203.0.113.0/24, 
          224.0.0.0/4, 
          240.0.0.0-255.255.255.255
        }
    }
    set byp6 {
        typeof ip6 daddr
        flags interval
        elements = { 
          ::, 
          ::1, 
          ::ffff:0:0:0/96, 
          64:ff9b::/96, 
          100::/64, 
          2001::/32, 
          2001:20::/28, 
          2001:db8::/32, 
          2002::/16, 
          fc00::/7, 
          fe80::/10, 
          ff00::-ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff
        }
    }
    chain tunnel_forward {
        ip daddr @byp4 counter accept
        ip6 daddr @byp6 counter accept
        meta skuid nobody counter accept
        #meta mark 0x2233 accept
        meta l4proto { tcp, udp } meta mark set 0x2233 counter comment "流量打上标签重新路由"
        #meta l4proto { tcp, udp } meta mark set 0x2233 
        #ip daddr @proxyv4 meta mark set 0x2233
        #tcp dport @proxy_port meta mark set 0x2233;
        #udp dport { 80, 443 } meta mark set 0x2233;
        #log prefix "TEST:  "
    }
    chain prerouting {
        type filter hook prerouting priority mangle; policy accept;
        iifname "br-lan" counter goto tunnel_forward
    }
    # Only for local mode
    chain output {
        type route hook output priority mangle; policy accept;
        oifname "eth1" counter goto tunnel_forward
    }
}
# uci get firewall.@defaults[0].forward   #openwrt环境里默认是reject的这里一定改成accept不然无法转发局域网设备的流量被坑惨了。以后配置规则一定要认真检查原有的防火墙规则！
# uci get network.lan.device #获取LAN接口
# uci get network.wan.device #获取WAN接口
