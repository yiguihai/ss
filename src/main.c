#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stddef.h>

#define BUF_SIZE 1024

int main(int argc, char *argv[]) {
  if (argc < 3) {
    printf("usage: %s <socket_file> <parametric>\n", argv[0]);
    return 0;
  }

  int sockfd;
  struct sockaddr_un cliun, serun;
  char buf[BUF_SIZE];
  int len;

  if ((sockfd = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0) {
    perror("Error opening stream socket");
    exit(EXIT_FAILURE);
  }

  memset(&cliun, 0, sizeof(cliun));
  cliun.sun_family = AF_UNIX;
  strcpy(cliun.sun_path, "/tmp/ss-client.socket");
  len = offsetof(struct sockaddr_un, sun_path) + strlen(cliun.sun_path);
  unlink(cliun.sun_path);

  if (bind(sockfd, (struct sockaddr *) &cliun, len) < 0) {
    perror("Error binding stream socket");
    exit(EXIT_FAILURE);
  }

  struct timeval tv;
  tv.tv_sec = 2;
  tv.tv_usec = 0;
  setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (const char *) &tv, sizeof tv);

  memset(&serun, 0, sizeof(serun));
  serun.sun_family = AF_UNIX;
  strcpy(serun.sun_path, argv[1]);
  len = offsetof(struct sockaddr_un, sun_path) + strlen(serun.sun_path);

  if (connect(sockfd, (struct sockaddr *) &serun, len) < 0) {
    perror("Error connecting stream socket");
    exit(EXIT_FAILURE);
  }

  if (write(sockfd, argv[2], strlen(argv[2])) < 0) {
    perror("Error writing on stream socket");
    exit(EXIT_FAILURE);
  }

  int n = read(sockfd, buf, BUF_SIZE - 1);
  if (n < 0) {
    if (errno == EAGAIN || errno == EWOULDBLOCK) {
      printf("Read operation timed out\n");
    } else {
      perror("Error reading stream message");
      exit(EXIT_FAILURE);
    }
  } else {
    buf[n] = '\0';
    printf("%s", buf);
  }

  close(sockfd);
}
