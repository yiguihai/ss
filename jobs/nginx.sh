#!/bin/bash
set -e

#bash jobs/cmake.sh
apt-get -qqy update
apt-get --yes install --no-install-recommends \
	git \
	jq \
	wget \
	ca-certificates \
	autoconf \
	libtool \
	libev-dev \
	autoconf \
	automake \
	build-essential \
	gcc \
	mercurial \
	m4 \
	binutils \
	pkg-config \
	libpcre3 \
	libpcre3-dev

cd /tmp
latest_version=$(wget -qO- https://api.github.com/repos/libunwind/libunwind/releases/latest | jq -r '.tag_name')
wget --no-check-certificate --quiet --continue https://github.com/libunwind/libunwind/releases/download/${latest_version}/libunwind-${latest_version:1}.tar.gz
tar -zxvf libunwind-${latest_version:1}.tar.gz
rm -f libunwind-${latest_version:1}.tar.gz
cd libunwind-${latest_version:1}
autoreconf -i
./configure --disable-documentation --disable-tests
make
make install
#git clone --recursive https://github.com/google/ngx_brotli /tmp/ngx_brotli
#cd /tmp/ngx_brotli && git submodule update --init
cd /tmp
latest_version="$(wget --no-check-certificate -qO- https://zlib.net/ | grep -oP 'zlib\-\d+.\d+(\.\d+)?\.tar.gz' | head -n1)"
wget --no-check-certificate --quiet --continue https://zlib.net/${latest_version}
echo ${latest_version}
tar -zxvf ${latest_version}
rm -f ${latest_version}
mv ${latest_version/.tar.gz/} zlib
cd /tmp/zlib
./configure --static --64
make
make install
#git clone --depth 1 https://boringssl.googlesource.com/boringssl
#https://blog.51cto.com/richmond/1572733 低版本系统需要打补丁
#patch -p0 boringssl/ssl/test/bssl_shim.cc ${CI_PROJECT_DIR:?}/patch/bssl_shim.patch
#mkdir -p boringssl/build boringssl/.openssl/lib
#cd boringssl/build
#cmake ..
#make
git clone --depth 1 https://github.com/quictls/openssl /tmp/openssl

hg clone https://hg.nginx.org/nginx /tmp/nginx
cd /tmp/nginx
./auto/configure \
	--prefix=/etc/ssmanager/usr \
	--user=nobody \
	--group=root \
	--with-pcre \
	--with-stream \
	--with-pcre-jit \
	--with-threads \
	--with-http_stub_status_module \
	--with-http_dav_module \
	--with-http_ssl_module \
	--with-stream_ssl_module \
	--with-stream_ssl_preread_module \
	--with-http_v2_module \
	--with-http_v3_module \
	--with-openssl=../openssl
	#--with-cc-opt="-Wno-error=type-limits -I../boringssl/include" \
	#--with-ld-opt="-L../boringssl/build/ssl -L../boringssl/build/crypto"
find ./ -name "Makefile" -type f -exec sed -i 's/-lpcre/-l:libpcre.a/g' {} +
make
make install
make clean
strip /etc/ssmanager/usr/sbin/nginx
bash ${CI_PROJECT_DIR:?}/jobs/push.sh
echo "$nginx_info" | base64 -d >>${CI_PROJECT_DIR:?}/temp/upgrade.log
mv -vf /etc/ssmanager/usr ${CI_PROJECT_DIR:?}/temp/nginx
cd ${CI_PROJECT_DIR:?}
sed -i "s/${nginx_old:?}/${nginx:?}/g" version/version
cp -vf temp/nginx/sbin/nginx usr/sbin/nginx
git add usr/sbin/nginx version/version temp/upgrade.log
git commit -m "更新nginx-quic"
git push origin HEAD:${CI_COMMIT_REF_NAME:?}
